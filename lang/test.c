#include <stdio.h>
#include <stdlib.h>

#include "lang.h"

void print_message(char *message)
{
	char *msg = get_lang(message);

	if (msg != NULL)
	{
		printf("%s\n", msg);
	}
	else
	{
		printf("%s\n", message);
	}

	free(msg);
}

int main()
{
	change_lang("es");

	print_message("HELLO");
	print_message("OK");

	destroy_lang();	
}
