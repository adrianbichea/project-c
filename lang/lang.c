#include <string.h>
#include <stdlib.h>

#include "lang.h"

char *language;

//change_lang("fr");, en, es
void change_lang(char *lang)
{
	language = malloc(strlen(lang) + 1);
	strcpy(language, lang);
}

char *get_lang(char *string)
{
	char *filename;
	char *fn = "language_";
	filename = malloc(strlen(fn) + strlen(language) + 2); 
	strcpy(filename, fn);
	strcat(filename, language);

	FILE *f = fopen(filename, "r");

	if (f == NULL)
	{
		printf("LANGUAGE FILE %s NOT FOUND!\n", filename);
		free(filename);
		return NULL;
	}

	int _feof = 0;
	char cod[10];
	char str[200];
	//get line
	unsigned char c;
	int idx = 0;
	int cnt = 0;
	while ((c = fgetc(f)) != EOF)
	{
		//printf("%d %d %d\n", c, idx, cnt);
		if (c != '\n')
		{
			if (c != '|')
			{
				if (cnt == 0)
				{
					cod[idx++] = c;
					cod[idx] = '\0';
				}
				else
				{
					str[idx++] = c;
					str[idx] = '\0';
				}
			}
			else
			{
				idx = 0;
				cnt++;
			}
		}
		else
		{
			idx = 0;
			cnt = 0;
			if (strcmp(cod, string) == 0)
			{
				char *s_ret;
				s_ret = malloc(strlen(str) + 1);
				strcpy(s_ret, str);
				free(filename);
				fclose(f);
				return s_ret;
			}
		}
	}

	fclose(f);
	free(filename);
	return NULL;
}

void destroy_lang()
{
	free(language);
}

