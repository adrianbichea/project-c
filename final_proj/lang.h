#ifndef _LANG_H_
#define _LANG_H_

#include <stdio.h>

void change_lang(char *);
char *get_lang(char *);
void destroy_lang();

#endif
