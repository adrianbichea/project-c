#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "annuaire.h"
#include "crypt.h"
#include "lang.h"
#include "util_printf.h"

int options_ok(char r, char min, char max)
{
	return (r >= min && r <= max) || r == 'q';
}

void show_languages()
{
	char *l[10] = {NULL};
	char r;	
	int i = 1;
	do
	{
		for (int x = 0; x < 10; x++)
		{
			if (l[x] != NULL)
			{
				//printf("free 26\n");
				free(l[x]);
			}
		}
		i = 1;
		PRINTLN(get_lang("MSG13"), RED);
		FILE *f = fopen("languages.txt", "r");
		while (1)
		{
			char lang[30];
			char enc[3];	
			fscanf(f, "%s %s", lang, enc);
			l[i - 1] = malloc(sizeof(enc) + 1);	
			strcpy(l[i - 1], enc);
			//printf("%s %d", l[i - 1], sizeof(l[i]));
			if (feof(f))
			{
				break;
			}
			char s[34];
			sprintf(s, "%d. %s", i, lang);
			PRINTLN(s, CYAN);
			//printf("%s %s %s\n", l[i - 1], lang, enc);

			i++;
		}
		fclose(f);
		PRINTLN(get_lang("MSG08"), CYAN);
		PRINT(get_lang("MSG09"), CYAN);
		r = fgetc(stdin);
		while (fgetc(stdin) != '\n');	
	} while (!options_ok(r, '1', 48 + i));
	if (r != 'q')
	{
		int idx = r - 48;
		//printf("%d .. %s\n", r, l[idx - 1]);
		change_lang(l[idx - 1]);
	}
	for (int a = 0; a < 10; a++)
	{
		if (l[a] != NULL)
		{
			//printf("free 68\n");
			free(l[a]);
		}
	}

	system("clear");
}

int main()
{
	system("clear");
	change_lang("en");	
	char r = '0';
	while (r != 'q')
	{
		fflush(stdin);
		do
		{
			PRINTLN(get_lang("WELCOME"), CYAN);
			PRINTLN(get_lang("MSG01"), CYAN);
			PRINTLN(get_lang("MSG02"), CYAN);
			
			PRINTLN(get_lang("MSG03"), GREEN);
			PRINTLN(get_lang("MSG04"), YELLOW);
			PRINTLN(get_lang("MSG05"), BLUE);
			PRINTLN(get_lang("MSG06"), MAGENTA);
			PRINTBKLN(get_lang("MSG07"), WHITE, BK_RED);
			PRINTLN(get_lang("MSG14"), CYAN);
			PRINTLN(get_lang("MSG08"), RED);
			PRINT(get_lang("MSG09"), CYAN);

			r = fgetc(stdin);
			while (fgetc(stdin) != '\n');
			
			system("clear");
		} while (!options_ok(r, '1', '6'));
		
		switch(r)
		{
			case '1' :
				add_new();
				break;
			case '2' :
				view(NULL);
				break;
			case '3' :
				search();
				break;
			case '4' :
				while (r != 'q')
				{
					fflush(stdin);
					do 
					{
						PRINTLN(get_lang("MSG01"), CYAN);
						PRINTLN(get_lang("MSG10"), GREEN);
						PRINTLN(get_lang("MSG11"), BLUE);
						PRINTLN(get_lang("MSG08"), RED);
						PRINTLN(get_lang("MSG09"), CYAN);

						r = fgetc(stdin);
						while (fgetc(stdin) != '\n');
						
						system("clear");
					} while (!options_ok(r, '1', '2'));

					switch(r) 
					{
						case '1':
							crypt();
							break;
						case '2':
							decrypt();
							break;
					}
				}
				r = '0';

				break;
				
			case '5' : 
				setup_coded();
				while (r != 'q')
				{
					do 
					{
						PRINTLN(get_lang("MSG12"), CYAN);
						PRINTLN(get_lang("MSG01"), CYAN);
						PRINTLN(get_lang("MSG03"), GREEN);
						PRINTLN(get_lang("MSG04"), YELLOW);
						PRINTLN(get_lang("MSG05"), BLUE);
						PRINTLN(get_lang("MSG08"), CYAN);
						PRINT(get_lang("MSG09"), CYAN);
						r = fgetc(stdin);
						while (fgetc(stdin) != '\n');
			
						system("clear");

					} while (!options_ok(r, '1', '3'));

					switch(r)
					{
						case '1' :
							add_new_coded();
							break;
						case '2' :
							view_coded(NULL);
							break;
						case '3' :
							search_coded();
							break;
					}
				}
				r = '0';

				break;
			case '6':
				show_languages();
				break;
		}

	}

}
