#include "crypt.h"

void show_menu();

int main()
{
	char r = '0';
	while (r != 'q')
	{
		show_menu();
		scanf(" %c", &r);
		fflush(stdin);

		switch(r)
		{
			case '1':
				crypt();
				break;
			case '2':
				decrypt();
				break;
			case '3':
				create_perroquet();
				break;
			case 'q':
				printf("\nQuitting...\n");
				break;
		}
	}

	return 0;
}

void show_menu()
{
	printf("\n *** Crypting & Decrypting ***\n");
	printf("\n1. Crypt file\n");
	printf("2. Decrypt file\n");
	printf("3. Define perroquet\n");
	printf("q. Quit\n");
	printf("\nYour option:");
}
