#include <stdio.h>

void show_switch(char *message, int num)
{
	printf("%s %d\n", message, num);
	switch(num)
	{
		case 0:
			printf("zero\n");
			break;
		case 1:
			printf("one\n");
			break;
		case 2:
			printf("two\n");
			break;
		case 3:
			printf("three\n");
			break;
		case 4:
			printf("for\n");
			break;
		default:
			printf("NOK\n");
			break;
	}
}

void method1(int x, int y)
{
	int s = x + y;
	show_switch("SUM: ", s);
}

void method2(int x, int y)
{
	int d = x - y;
	show_switch("DIF: ", d);
}

int main()
{
	method1(2, 2);
	method2(3, 4);
}
