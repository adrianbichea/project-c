#include <stdio.h>
#include <stdlib.h>
#include "annuaire.h"
#include "util_printf.h"

int options_ok(char r)
{
	return (r >= '1' && r <= '7') || r == 'q';
}

void show_option_disable_message()
{
	PRINTBKLN("Option disabled!", BK_RED, YELLOW);
	PRINTLN("Please, setup 'perroquet' and coded file first.", RED);
}

int main()
{
	system("clear");

	char r = '0';
	while (r != 'q')
	{
		fflush(stdin);
		do
		{
			printf("\nMENU\n\n");
			printf("\n1. Add\n");
			printf("2. View\n");
			printf("3. Search\n");
			printf("\n");
			int color = is_coded_set() ? GREEN : YELLOW;
			int bk_color = is_coded_set() ? 0 : BK_RED;
			PRINTBKLN("4. Add to coded file", color, bk_color);
			PRINTBKLN("5. View from coded file", color, bk_color);
			PRINTBKLN("6. Search from coded file", color, bk_color);
			PRINTLN("7. Setup perroquet and coded file", GREEN);
			printf("\n");
			printf("q. Quit\n");
			printf("\nOption:");
	
			r = fgetc(stdin);
			while (fgetc(stdin) != '\n');
			system("clear");
		} while (!options_ok(r));

		switch(r)
		{
			case '1':
				add_new();
				break;
			case '2':
				view(NULL);
				break;
			case '3':
				search();
				break;
			case '4':
				if (is_coded_set())
				{
					add_new_coded();
				}
				else
				{
					show_option_disable_message();
				}
				break;	
			case '5':
				if (is_coded_set())
				{
					view_coded(NULL);
				}
				else
				{
					show_option_disable_message();
				}
				break;
			case '6':
				if (is_coded_set())
				{
					search_coded();
				}
				else
				{
	 				show_option_disable_message();
	 			}
				break;
			case '7':
				setup_coded();
				break;
		}
	}
	destroy_annuaire();
}
