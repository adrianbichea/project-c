#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "annuaire.h"
#include "coded.h"
#include "input_help.h"

#define FILENAME "annuaire.txt"
#define STRING_MAX_SIZE 256

int str_compare(char *source, char *find);
char *perroquet_filename = NULL;
char *coded_filename = NULL;
int coded_set = 0;

int add_new()
{
	fflush(stdin);
	char *nom = get_input("\nNom: ", 100);
	char *prenom = get_input("Prenom: ", 100);
	char *telef = get_input("Tel: ", 20);

	FILE *f = fopen(FILENAME, "a+");
	fprintf(f, "%s:%s:%s\n", nom, prenom, telef);
	fclose(f);

	free(nom);
	free(prenom);
	free(telef);
}

void view(char *criteria)
{
	FILE *f = fopen(FILENAME, "r");
	if (f == NULL)
	{
		return;
	}
	
	if (criteria != NULL)
	{
		printf("\n ** Search users (%s) **\n\n", criteria);
	}
	else
	{
		printf("\n ** View all users **\n\n");
	}
	char c;
	char *line = NULL;
	int buffer_size = 200;
	char buffer[buffer_size];
	int index = 0;
	s_phone_user user;
	while ((c = fgetc(f)) != EOF)
	{
		if (c != '\n')
		{
			buffer[index++] = c;
		}
		else
		{
			buffer[index++] = '\0';
			line = malloc(index + 1);
			strcpy(line, buffer);

			char *t = strtok(line, ":");
			int idx = 0;
			while (t != NULL)
			{
				switch(idx)
				{
					case 0:
						strcpy(user.nom, t);
						break;
					case 1:
						strcpy(user.prenom, t);
						break;
					default:
						strcpy(user.tel, t);
				}
				idx++;
				t = strtok(NULL, ":");
			}
			if (criteria != NULL)
			{
				int found = 0;
				if (str_compare(user.nom, criteria) == 0)
				{	
					found = 1;
				}
				if (str_compare(user.prenom, criteria) == 0)
				{
					found = 1;
				}
				if (str_compare(user.tel, criteria) == 0)
				{
					found = 1;
				}
				if (found)
				{
					printf("[%s] [%s] - [%s]\n", 
							user.nom, user.prenom, user.tel);
				}
			}
			else
			{
				printf("[%s] [%s] - [%s]\n", user.nom, user.prenom, user.tel);
			}
			free(t);
			free(line);
			line = NULL;
			index = 0;
		}

	}
	fclose(f);
}

int str_compare(char *source, char *find)
{
	int begin = 0;
	int end = 0;

	if (find[0] == '*')
	{
		begin = 1;
	}
	if (find[strlen(find) - 1] == '*')
	{
		end = 1;
	}

	if (strlen(find) > strlen(source))
	{
		return 1;
	}

	if (begin == 0 && end == 0)
		return strcmp(source, find);

	if (begin == 1 && end == 0)
	{
		int k = strlen(find) - 1;
		for (int i = strlen(source) - 1; i >= 0; i--)
		{
			if (source[i] != find[k])
			{
				return find[k] == '*' ? 0 : 1;
			}
			k--;
		}
		return 0;
	}

	if (begin == 0 && end == 1)
	{
		for (int i = 0; i < strlen(source); i++)
		{
			if (source[i] != find[i])
			{
				return find[i] == '*' ? 0 : 1;
			}
		}
		return 0;
	}

	for (int i = 0; i < strlen(source); i++)
	{
		int found = 1;
		int k = 0;
		for (int j = 0; j < strlen(find); j++)
		{
			if (i + strlen(find) - 2 > strlen(source))
			{
				return 1;//not found, string overflow
			}
			if (find[j] != '*')
			{
				if (source[i + k] != find[++k])
				{
					found = 0;
					break;
				}
			}
		}
		if (found)
		{
			return 0;
		}
	}
	return 1;
}

char *search_criteria()
{
	printf("\nSearch use...\n");
	printf("  *word - search through DB all records that ends with 'word'\n");
	printf("  word* - search through DB all records that begins with 'word'\n");
	printf("  *word* - search through DB all records with 'word' in\n\n");
	char *criteria;
	do
	{
		//printf("Search (min 2 char length): ");
		//scanf("%s", &criteria);
		criteria = get_input("Search (min 2 char length): ", 100);
		fflush(stdin);
	} while (strlen(criteria) < 2);
	return criteria;
}

void search()
{
	char *criteria = search_criteria();
	view(criteria);
	free(criteria);
}

int add_new_coded()
{
	fflush(stdin);

	if (perroquet_filename == NULL)
	{
		char *peroq_file = get_input("Provide 'perroquet' file: ", 250);
		coded_set_perroquet(peroq_file);

		char *code_file = get_input("Provide coded file: ", 250);
		coded_open(code_file, "rb");
		free(peroq_file);
		free(code_file);
	}
	else
	{
		coded_set_perroquet(perroquet_filename);
		coded_open(coded_filename, "rb");
	}

	char *nom = get_input("\nNom: ", 100);
	char *prenom = get_input("Prenom: ", 100);
	char *telef = get_input("Tel: ", 20);

	char str[256];
	sprintf(str, "%s:%s:%s\n", nom, prenom, telef);
	coded_add_line(str);
	coded_close();

	free(nom);
	free(prenom);
	free(telef);
}

void view_coded(char *criteria)
{
	printf("\nView coded\n");

	if (perroquet_filename == NULL)
	{
		char *peroq_file = get_input("Provide 'perroquet' file: ", 250);
		coded_set_perroquet(peroq_file);

		char *code_file = get_input("Provide coded file: ", 250);
		coded_open(code_file, "rb");
		free(peroq_file);
		free(code_file);
	}
	else
	{
		coded_set_perroquet(perroquet_filename);
		coded_open(coded_filename, "rb");
	}
	s_phone_user user;
	while (!is_eof())
	{
		char *line = coded_next_line();
		if (is_eof())
		{
			continue;
		}
		char *t = strtok(line, ":");
		int idx = 0;
		while (t != NULL)
		{
			switch(idx)
			{
				case 0:
					strcpy(user.nom, t);
					break;
				case 1:
					strcpy(user.prenom, t);
					break;
				default:
					strcpy(user.tel, t);
			}
			idx++;
			t = strtok(NULL, ":");
		}
		free(line);

		if (criteria != NULL)
		{
			int found = 0;
			if (str_compare(user.nom, criteria) == 0)
			{	
				found = 1;
			}
			if (str_compare(user.prenom, criteria) == 0)
			{
				found = 1;
			}
			if (str_compare(user.tel, criteria) == 0)
			{
				found = 1;
			}
			if (found)
			{
				printf("[%s] [%s] - [%s]\n", user.nom, user.prenom, user.tel);
			}
		}
		else
		{
			printf("[%s] [%s] - [%s]\n", user.nom, user.prenom, user.tel);
		}
	}
	coded_close();
}

void search_coded()
{
	char *criteria = search_criteria();
	view_coded(criteria);
	free(criteria);
}

void setup_coded()
{
	if (perroquet_filename != NULL)
	{
		free(perroquet_filename);
		free(coded_filename);
	}
	perroquet_filename = get_input("Provide 'perroquet' file: ", 250);
	coded_filename = get_input("Provide coded file: ", 250);
	coded_set = 1;
}

int is_coded_set()
{
	return coded_set;
}

void destroy_annuaire()
{
	if (perroquet_filename != NULL)
	{
		free(perroquet_filename);
	}
	if (coded_filename != NULL)
	{
		free(coded_filename);
	}
}
