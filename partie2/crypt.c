#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crypt.h"
#include "input_help.h"

#define CRYPT -1
#define DECRYPT 1

#define BUF_SIZE 16

int MIN_CODE = 32;
int MAX_CODE = 125;

int crypt_file(char *src_file, char *dest_file, char *code_file, int mode)
{
	FILE *f_src = fopen(src_file, "rb");
	if (f_src == NULL)
	{
		return 0;//NOT OK
	}
	
	FILE *f_dst = fopen(dest_file, "wb");
	if (f_dst == NULL)
	{
		fclose(f_src);
		return 0;
	}

	FILE *f_code = fopen(code_file, "rb");
	if (f_code == NULL)
	{
		fclose(f_src);
		fclose(f_dst);
		return 0;
	}

	unsigned char buf[BUF_SIZE] = {0};
	size_t bytes = 0;
	size_t read_size = sizeof buf;
	int i = 0;
	unsigned char cod[BUF_SIZE] = {0};
	unsigned char peroq[1] = {0};
	while ((bytes = fread(buf, sizeof *buf, read_size, f_src)) == read_size)
	{
		for (i = 0; i < read_size; i++)
		{
			if (fread(peroq, sizeof *peroq, 1, f_code) == 0)
			{
				fclose(f_code);
				f_code = fopen(code_file, "rb");
				fread(peroq, sizeof *peroq, 1, f_code);
			}
			cod[i] = buf[i] + (peroq[0] * mode);
		}
		fwrite(cod, sizeof(char), read_size, f_dst);
	}
	for (i = 0; i < bytes; i++)
	{
		if (fread(peroq, sizeof *peroq, 1, f_code) == 0)
		{
			fclose(f_code);
			f_code = fopen(code_file, "rb");
			fread(peroq, sizeof *peroq, 1, f_code);
		}
		cod[i] = buf[i] + (peroq[0] * mode);
	}
	
	if (bytes != 0)
	{
		fwrite(cod, sizeof(char), bytes, f_dst);
	}

	fclose(f_src);
	fclose(f_dst);
	fclose(f_code);

	if (mode == CRYPT)
	{
		remove(src_file);
	}
	return 1;
}

void crypt()
{
	printf("\n** ENCRYPTING **\n");
	char *src_file = get_input("Enter source file (.txt): ", 256);
	char *dst_file = get_input("Enter destination file (.crt): ", 256);
	char *code_file = get_input("Enter 'perroquet' file (.def): ", 256);

	printf("Do action: %s %s %s\n", src_file, dst_file, code_file);

	if (crypt_file(src_file, dst_file, code_file, CRYPT))
	{
		printf("\n   *** Encrypted ** \n");
	}
	free(src_file);
	free(dst_file);
	free(code_file);
}

void decrypt()
{
	printf("\n** DECRYPT **\n");
	char *src_file = get_input("Enter source file (.crt): ", 256);
	char *dst_file = get_input("Enter destination file (.txt): ", 256);
	char *code_file = get_input("Enter 'perroquet' file (.def): ", 256);
	if (crypt_file(src_file, dst_file, code_file, DECRYPT))
	{
		printf("\n   *** Decrypted ***\n");
	}
	free(src_file);
	free(dst_file);
	free(code_file);
}

void create_perroquet()
{
	char *code = get_input("Enter 'peroquet' phrase (max 255): ", 256);

	char *peroq = get_input("Enter 'perroquet' file (.def):", 256);

	FILE *f = fopen(peroq, "w");
	fprintf(f, "%s", code);
	fclose(f);

	free(code);
	free(peroq);

}
